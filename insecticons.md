# Characters
- Wave 1 - [UT 10/T40](https://www.pojo.com/wp-content/uploads/2018/08/ut10-chopshop-both.jpg)	**Chop Shop** - *Sneaky Insecticon*
- Wave 1 - [RT 23/T40](https://www.pojo.com/wp-content/uploads/2018/08/rt-23-both-insecticon-skrapnel.jpg)	**Insecticon Skrapnel** - *Insecticon Leader*
- Wave 1 - [UT 25/T40](https://www.pojo.com/wp-content/uploads/2018/09/kickback-transformer.jpg) **Kickback** - *Cunning Insecticon*
- Wave 1 - [UT 33/T40](https://www.pojo.com/wp-content/uploads/2018/09/ransack-ut33.jpg) **Ransack** - *Insecticon Commando*

# Battle Cards
- Wave 1 - [C020/081](https://www.pojo.com/wp-content/uploads/2018/09/data-pad-transformers-tcg.jpg)  **Data Pad** **x3**
- Wave 1 - [C060/081](https://www.pojo.com/wp-content/uploads/2018/10/Ready-For-Action-Transformers.jpg) **Ready For Action** **x2**
- Wave 1 - [R077/081](https://www.pojo.com/wp-content/uploads/2018/10/The-Bigger-They-Are-Transformers.jpg)  **The Bigger They Are** **x2**
- Wave 1 - [U055/081](https://www.pojo.com/wp-content/uploads/2018/08/power-sword-u55.jpg)  **Power Sword** **x3**
- Wave 1 - [R050/081](https://www.pojo.com/wp-content/uploads/2018/10/One-Shall-Stand-One-Shall-Fall.jpg)  **One Shall Stand, One Shall Fall** **x3**
- Wave 1 - [C062/081](https://www.pojo.com/wp-content/uploads/2018/10/Repair-Bay-Transformers.jpg)  **Repair Bay** **x3**
- Wave 1 - [R051/081](https://www.pojo.com/wp-content/uploads/2018/08/peace-through-tyranny.jpg)  **Peace through Tyranny** **x3**
- Wave 1 - [R075/081](https://www.pojo.com/wp-content/uploads/2018/10/System-Reboot-Transformers.jpg) **System Reboot** **x3**
- Wave 1 - [R028/081](https://www.pojo.com/wp-content/uploads/2018/10/Energon-Axe.jpg)  **Energon Axe** **x2**
- Wave 1 - [R037/081](https://www.pojo.com/wp-content/uploads/2018/09/i-still-funtion-transformers-tcg.jpg)  **I Still Function!** **x1**
- Wave 1 - [C038/081](https://www.pojo.com/wp-content/uploads/2018/10/Improvised-Shield.jpg)  **Improvised Shield** **x3**
- Wave 1 - [R074/081](https://www.pojo.com/wp-content/uploads/2018/10/swarm-transformers.jpg)  **Swarm!** **x3**
- Wave 1 - [U011/081](https://www.pojo.com/wp-content/uploads/2018/09/bug-bomb-transformers-tcg.jpg)  **Bug Bomb** **x2**
- Wave 1 - [C053/081](https://www.pojo.com/wp-content/uploads/2018/10/Piercing-Blaster-Transformers.jpg)  **Piercing Blaster** **x3**
- Wave 1 - [U033/081](https://www.pojo.com/wp-content/uploads/2018/08/grenade-launcher-transformers.jpg)  **Grenade Launcher** **x2**
- Wave 1 - [U079/081](https://www.pojo.com/wp-content/uploads/2018/10/Treasure-Hunt-Transformers.jpg)  **Treasure Hunt** **x1**
- Wave 1 - [R001/002](https://www.pojo.com/wp-content/uploads/2018/09/all-out-attack-transformers.jpg) **All Out Attack** **x1**