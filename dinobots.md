# Characters
- Wave 1 - [RT 21/T40](https://www.pojo.com/wp-content/uploads/2018/09/Grimlock-Dinobot-Leader.jpg)	**Grimlock** - *Dinobot Leader*
- Wave 1 - [UT 18/T40](https://www.pojo.com/wp-content/uploads/2018/08/ut-t18-both-dinobot-snarl.jpg)	**Dinobot Snarl** - *Desert Warrior*
- Wave 1 - [UT 16/T40](https://www.pojo.com/wp-content/uploads/2018/08/Ut-t16-Dinosaur-Sludge-Both.jpg) **Dinobot Sludge** - *Mighty Stomper*

# Battle Cards
- Wave 1 - [R078/081](https://www.pojo.com/wp-content/uploads/2018/10/Thermal-Weaponry-Transformers.jpg)  **Thermal Weaponry x1**
- Wave 1 - [R044/081](https://www.pojo.com/wp-content/uploads/2018/08/matrix-of-leadership.jpg)  **Matrix of Leadership x1**
- Wave 1 - [R022/081](https://www.pojo.com/wp-content/uploads/2018/10/dino-chomp-transformers.jpg)  **Dino-Chomp! x3**
- Wave 1 - [R014/081](https://www.pojo.com/wp-content/uploads/2018/10/Combat-Training-Transformers.jpg)  **Combat Training x3**
- Wave 1 - [U079/081](https://www.pojo.com/wp-content/uploads/2018/10/Treasure-Hunt-Transformers.jpg)  **Treasure Hunt x1**
- Wave 1 - [U055/081](https://www.pojo.com/wp-content/uploads/2018/08/power-sword-u55.jpg)  **Power Sword** **x3**
- Wave 1 - [U033/081](https://www.pojo.com/wp-content/uploads/2018/08/grenade-launcher-transformers.jpg)  **Grenade Launcher** **x1**
- Wave 1 - [U057/081](https://www.pojo.com/wp-content/uploads/2018/10/Ramming-Speed-Transformers.jpg)  **Ramming Speed x3**
- Wave 1 - [U008/081](https://www.pojo.com/wp-content/uploads/2018/09/body-armor-transformers-tcg-1.jpg)  **Body Armor x3**
- Wave 1 - [U042/081](https://www.pojo.com/wp-content/uploads/2018/10/Jaws-of-Steel-Transformers.jpg)  **Jaws of Steel x3**
- Wave 1 - [C060/081](https://www.pojo.com/wp-content/uploads/2018/10/Ready-For-Action-Transformers.jpg) **Ready For Action** **x1**
- Wave 1 - [C059/081](https://www.pojo.com/wp-content/uploads/2018/10/Rapid-Conversion-Transformers.jpg)  **Rapid Conversion x2**
- Wave 1 - [C061/081](https://www.pojo.com/wp-content/uploads/2018/10/Reinforced-Plating-Transformers.jpg)  **Reinforced Plating x3**
- Wave 1 - [C071/081](https://www.pojo.com/wp-content/uploads/2018/10/Supercharge-Transformers.jpg)  **Supercharge x3**
- Wave 1 - [C056/081](https://www.pojo.com/wp-content/uploads/2018/10/Primary-Laser-Transformers.jpg)  **Primary Laser x3**
- Wave 1 - [C030/081](https://www.pojo.com/wp-content/uploads/2018/10/Flamerthrower-Transformers.jpg)  **Flamethrower x3**
- Wave 1 - [C043/081](https://www.pojo.com/wp-content/uploads/2018/08/c043-Leap-into-Battle.jpg)  **Leap Into Battle x3**